const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();
const port = 8000;
const albums = require('./app/Albums');
const artists = require('./app/Artists');
const tracks = require('./app/Tracks');

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const run = async () => {
    await mongoose.connect("mongodb://localhost/musicApi", {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

    console.log("Connected to Mongo DB");

    app.use('/albums', albums());
    app.use('/artists', artists());
    app.use('/tracks', tracks());

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);
