const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Artist = require('../models/Artist');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get("/", async (req, res) => {
        try {
            const artists = await Artist.find();
            res.send(artists);
        } catch (e) {
            res.sendStatus(500);
        }
    });
    router.post("/", upload.single("image"), async (req, res) => {
        const artistData = new Artist(req.body);
        if (req.file) {
            artistData.image = req.file.filename;
        }
        try {
            await artistData.save();
            res.send(artistData);
        } catch (e) {
            res.sendStatus(400);
        }
    });
    return router;
};

module.exports = createRouter;