const express = require('express');
const router = express.Router();
const Tracks = require("../models/Track");

const createRouter = () => {
    router.get("/", async (req, res) => {
        let track;
        try {
            if (req.query.album) {
                track = await Tracks.find({"album": req.query.album}).populate({path: 'album', populate: {path: 'artist'}});
            } else {
                track= await Tracks.find().populate({path: 'album', populate: {path: 'artist'}}); }
            res.send(track);
        } catch (e) {
            res.sendStatus(500);
        }
    });
    router.post("/", async (req, res) => {
        const trackData =  new Tracks(req.body);
        try {
            await trackData.save();
            res.send(trackData);
        } catch (e) {
            res.sendStatus(400);
        }
    });
    return router;
};

module.exports = createRouter;